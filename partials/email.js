const input = require('../input.json');

module.exports = {
    generate: function () {
      return `
        <?php if ($_POST['FormSubmit'] === "GeneratedForm") {
            //these variables are used only in an email
            $_POST['FirstName'] = ucwords(trim($_POST['FirstName']));
            $_POST['LastName'] = ucwords(trim($_POST['LastName']));
            $from_email = strtolower(trim($_POST['Email']));
            $email_confirm = strtolower(trim($_POST['email_confirm']));
            $phone = preg_replace("/\D/", '', $_POST['Phone']);
            ${generateVars()}

            //Note: since payment may have already been collected, we really need to let almost everything through to email
            //however, we'll validate the email and phone fields, because failing those is a pretty common spambot mistake
            //if valid data
            if ((!$from_email
                || filter_var($from_email, FILTER_VALIDATE_EMAIL)
                && $from_email == $email_confirm)
              && (!$phone || strlen($phone) == 10)
              && ($via_paypal || valid_or_disabled_captcha($_POST))
              && ((!$via_paypal && empty($_POST['payment']))
			      	  || ($via_paypal && $_POST['payment'] === 'valid'))
                ${generatePaymentChecks()}
            ) {
              $from_name = format_name($_POST);
              $to_address = '${input.form.to}';
              $subject = 'New ${input.form.title} Submission';
              $message = "<html><body><h1>${input.form.title} Submission!</h1>Someone has submitted the ${input.form.title} form. Here's the submission:<br><br><strong>Name:</strong> $from_name<br>\n";
              //loop through all posted fields
              foreach ($_POST as $label => $value) {
                //exclude name fields (already handled above), and FormSubmit fields
                if (strpos($label, 'Name') === false
                  && !in_array($label, array('Suffix', 'FormSubmit', 'g-recaptcha-response'))
                ) {
                  //replace underscores in both the label and value and uppercase the first letter of each word
                  $label = ucwords(str_replace('_', ' ', $label));
                  $value = ucwords(filter_var(trim(htmlspecialchars_decode(str_replace('_', ' ', $value))), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));
                  $message .= "<strong>$label:</strong> $value<br>";
                }
              }
              $message .= '</body></html>';
              if ($app['helper.mail']->send($to_address, $subject, $message, $from_name, $from_email)) {
                ?> 
                <h1>Form Submitted</h1>
                <div class="copy">
                  <p>The form has been submitted successfully. We will be in touch soon.</p>
                </div>
                <?php
              }
              else {
                //show server error notification
                $app['noty'] = create_noty('error', '<strong>Error sending message.</strong>');
              }
            }
            else {
              require_once $app['APP_ROOT'] . 'includes/invalid_data.php';
              $app['noty'] = create_noty('error');
            }
        } ?>
      `
    }
};

const generateVars = () => {
  // Get all fields that have paid options
  const fields = input.bricks.flatMap(i => i.groups).flatMap(i => i.fields)
  .filter(i => i.options && i.options.find(j => j.price && j.price !== 0));

  let builder = '';

  fields.map(field => {
    const costlyOptions = fields.flatMap(field => field.options.filter(option => option.price && option.price !== 0));
    builder += `$paid_${field.name}_types = array(${JSON.stringify(costlyOptions.map(i => i.value)).replace('[', '').replace(']', '')});\n`;
    builder += `$via_paypal = (empty($_POST['${field.name}']) || !in_array($_POST['${field.name}'], $paid_${field.name}_types)) ? false : true;`;
  })

  return builder;
}

const generatePaymentChecks = () => {

  let builder = '';

  // Get all fields that have paid options
  const fields = input.bricks.flatMap(i => i.groups).flatMap(i => i.fields)
  .filter(i => i.options && i.options.find(j => j.price && j.price !== 0));

  fields.map(field => {
    if (field.required) { builder += `&& !empty($_POST['${field.name}'])` }
  })
  return builder;
}