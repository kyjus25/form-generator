const input = require('../input.json');

module.exports = {
    generate: function () {
      return `
            <div class="formgroup" style="text-align:center;">
                <?php required_footnote();?>
			    <input type="hidden" name="FormSubmit" value="GeneratedForm">
                <label id="generated_form_submit" style="width: 300px; margin: auto; display: none;" class="custom_button">Submit</label>
                <div style="width: 300px; margin: auto; display: none;" id="paypal-button-container"></div>
                <div id="captcha" class="formgroup">
                        <?php captcha_render();?>
                </div>
            </div>
        </form>
        <script>
            function checkRadio(ele) {
                // Expand the section that correlates to that radio
                const element = $("input[name=" + ele + "]:checked").val() + '_section';
                $(".depends_" + ele).hide();
                $("#" + element).show();
                $('.masonry').masonry();

                // Decide whether to show the paypal or submit button
                const activeField = $("input[name=" + ele + "]:checked");
                if (!activeField.val()) { showButton(); }
                if (activeField.val() && activeField.data('price') === 0) { showButton(); }
                if (activeField.val() && activeField.data('price') !== 0) { showPaypal(activeField.data('price'), activeField.data('label')); }

                // Check whether any input items should be disabled
                $(':input').prop('disabled', false);
                $(':input').filter(':not(:visible)').prop('disabled', true);
            }

            function showButton() {
                $("#generated_form_submit").show();
                $('#captcha').show();
                $("#paypal-button-container").hide();
            }

            function showPaypal(price, label) {
                paypal_price = price;
                paypal_item = label;
                $("#generated_form_submit").hide();
                $('#captcha').hide();
                $("#paypal-button-container").show();
            }
        </script>
        <script>
            ${generateChangeEvents()}
            ${generateValidation()}
        </script>
    </div>
    <script src="https://www.paypal.com/sdk/js?client-id=${input.paypal.client}&currency=USD" data-sdk-integration-source="button-factory"></script>
    <script>
        let paypal_actions = null;
        let paypal_price = 0;
        let paypal_item = '';

        function initPayPalButton() {
        paypal.Buttons({
            style: {
                shape: 'pill',
                tagline: false,
                color: 'gold',
                layout: 'horizontal',
                label: 'paypal'
            },

            onInit: function(data, actions)  {
                paypal_actions = actions;
                actions.disable();
            },

            onClick: function(data, actions) {
                return new Promise(function(resolve, reject) {
                    if (!valid()) {
                        <?php echo create_noty();?>
                        resolve(actions.reject());
                    } else {
                        resolve(actions.resolve());
                    }
                });
                
            },

            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: [{"description": paypal_item,"amount":{"currency_code":"USD","value": paypal_price}}]
                });
            },

            onApprove: function(data, actions) {
                return actions.order.capture().then(function(orderData) {
                    $("#generated_form").append('<input type="hidden" name="payment" value="valid">');
					$( "#generated_form" ).submit();
                });
            },

            onError: function(err) {
                <?php echo create_noty('error'); ?>
            }
        }).render('#paypal-button-container');
        }
        initPayPalButton();
    </script>
        <?php } ?>
      `;
    }
};

const generateChangeEvents = () => {
    // Get all fields that have dependents, and make sure there are no duplicate entries
    let changeElements = input.bricks
        .filter(brick => brick.depends && brick.depends !== '')
        .map(brick => brick.depends.split('.')[0]);
    changeElements = changeElements.filter((e, i) => changeElements.indexOf(e) === i);
    
    return changeElements.map(element => `
        // Calculate the initial page state for radio buttons
        $( document ).ready(() => {
            // Wait until the page loads to ensure masonry has initialized
            checkRadio("${element}");
        });

        // Change events for radio buttons
        $('#${element}').change(function() {
            checkRadio("${element}");
        });
    `);
}

const generateChecks = () => {
    return input.bricks.flatMap(i => i.groups).flatMap(i => i.fields).filter(i => i.required).map(requiredField => {
        
        switch (requiredField.type) {
            case 'email':
                return `

                    if ( $('[name=${requiredField.name}]').is(":visible") ) {
                        if (!emailRegex.test(${requiredField.name}.value.trim())) {
                            // error if ${requiredField.name} empty or not valid email
                            if (shouldMark) { $('[name=${requiredField.name}]').addClass(warning_class); }
                            valid = false;
                        } else if ( ${requiredField.name}.value.trim() != email_confirm.value.trim() ) {
                            // Error if the emails don't match
                            if (shouldMark) {
                                $('[name=${requiredField.name}]').addClass(warning_class);
                                $('[name=email_confirm').addClass(warning_class);
                            }
                            valid = false;
                        } else {
                            $('[name=${requiredField.name}]').removeClass(warning_class); 
                            $('[name=email_confirm').removeClass(warning_class);
                        }
                    } else {
                        $('[name=${requiredField.name}]').removeClass(warning_class); 
                        $('[name=email_confirm').removeClass(warning_class);
                    }
                `;
            case 'number':
                return `
                    // error if ${requiredField.name} empty or not valid number
                    if (!intRegex.test(${requiredField.name}.value.trim()) && $('[name=${requiredField.name}]').is(":visible") ) {
                        if (shouldMark) { $('[name=${requiredField.name}]').addClass(warning_class); }
                        valid = false;
                    } else {
                        $('[name=${requiredField.name}]').removeClass(warning_class);
                    }
                `;
            case 'zip':
                return `
                    // error if ${requiredField.name} empty or not valid zip or number
                    if ((${requiredField.name}.value.length < 5 || !intRegex.test(${requiredField.name}.value)) && $('[name=${requiredField.name}]').is(":visible")) {
                        if (shouldMark) { $('[name=${requiredField.name}]').addClass(warning_class); }
                        valid = false;
                    } else {
                        $('[name=${requiredField.name}]').removeClass(warning_class);
                    }
                `;
            case 'city':
                return `
                    // error if ${requiredField.name} empty or not valid city
                    if (!cityRegex.test(${requiredField.name}.value.trim()) && $('[name=${requiredField.name}]').is(":visible") ) {
                        if (shouldMark) { $('[name=${requiredField.name}]').addClass(warning_class); }
                        valid = false;
                    } else {
                        $('[name=${requiredField.name}]').removeClass(warning_class);
                    }
                `;
            case 'radio':
                return `
                    //error if no ${requiredField.name} selected
                    if (!$('#${requiredField.name} input:checked').val()) {
                        if (shouldMark) { $('#${requiredField.name}').addClass(warning_class); }
                        valid = false;
                    } else {
                        $('#${requiredField.name}').removeClass(warning_class);
                    }
                `;
            case 'phone':
                return `
                    //error if ${requiredField.name} provided and improper phone format
                    if ( $('[name=${requiredField.name}]').is(":visible") && (${requiredField.name}.value.replace(/[^0-9]/g, '').length != 10
                        || ${requiredField.name}.value.replace(/[^0-9]/g, '') == '0000000000')
                    ) {
                        if (shouldMark) { $('[name=${requiredField.name}]').addClass(warning_class); }
                        valid = false;
                    } else {
                        $('[name=${requiredField.name}]').removeClass(warning_class);
                    }
                `;
            case 'any':
                return `
                    //error if ${requiredField.name} empty
                    if (!${requiredField.name}.value.trim() && $('[name=${requiredField.name}]').is(":visible")) {
                        if (shouldMark) { $('[name=${requiredField.name}]').addClass(warning_class); }
                        valid = false;
                    } else {
                        $('[name=${requiredField.name}]').removeClass(warning_class);
                    }
                `;
            default:
                return `
                    // error if ${requiredField.name} empty or not only allowed characters
                    if (!nameRegex.test(${requiredField.name}.value.trim()) && $('[name=${requiredField.name}]').is(":visible") ) {
                        if (shouldMark) { $('[name=${requiredField.name}]').addClass(warning_class); }
                        valid = false;
                    } else {
                        $('[name=${requiredField.name}]').removeClass(warning_class);
                    }
                `;
        }
        
    }).join('');
}

const generateValidation = () => {
    return `
        $( "#generated_form_submit" ).click(e => {
            e.preventDefault();
            if (!valid()) {
                <?php echo create_noty();?>
            } else {
                <?php if ($app['config']['captcha']['enabled']) { ?>
                    grecaptcha.execute();
                <?php } else { ?>
                    $( "#generated_form" ).submit();
                <?php } ?>
            }
        });

        $("#generated_form :input").change(function() {
            if (!valid(false)) {
                paypal_actions.disable();
            } else {
                paypal_actions.enable();
            }
        });

        function valid(shouldMark = true) {
            const warning_class = 'mark';
            const intRegex = <?php echo _REGEX_INTEGER_JS;?>;
			const nameRegex = <?php echo _REGEX_NAME;?>;
			const cityRegex = <?php echo _REGEX_CITY;?>;
			const emailRegex = <?php echo _REGEX_EMAIL_JS;?>;
            let valid = true;

            $(':input').prop('disabled', false);
            $(':input').filter(':not([type=hidden])').filter(':not(:visible)').prop('disabled', true);

            with (document.generated_form) {
                ${generateChecks()}
            }

            return valid;
        }
    `
}