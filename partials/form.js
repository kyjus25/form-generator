const input = require('../input.json');

module.exports = {
    generate: function () {
      return    `
                <?php if (!$_POST || $_POST['FormSubmit'] !== "GeneratedForm") {?>
                <h1><?php echo $metatitle;?></h1>
                <form method="post" name="generated_form" id="generated_form" class="copy cms-form">
		            <div class="masonry cards">
                        ${input.bricks.map(brick => generateBrick(brick) ).join('')}
                    </div>`;
                // Note that the form is not closed. This is because it is closed in validation.js later

    }
};

const generateBrick = (brick) => {
    return  `<div id="${brick.depends ? brick.depends?.split('.')[1] : ''}_section" class="brick card depends_${brick.depends ? brick.depends?.split('.')[0] + ' hide' : ''}">
                <h2>${brick.label}</h2>
                ${brick.groups.map(group => generateGroup(group) ).join('')}
            </div>`;
}

const generateGroup = (group) => {
    return (group.label ? `<div class="formgroup">` : '') +
                (group.label ? `<div class="label"><strong>${group.label}${generateRequired(group)}</strong></div>` : '') +
                '<div class="columns">' +
                    group.fields.map(field => generateField(field, group.columns) ).join('') +
                '</div>' +
            (group.label ? '</div>' : '')
}

const generateField = (field, columns) => {
    return `<div id="${field.type === 'radio' ? field.name : ''}" class="${columns !== 1 ? 'col col-' + (12 / columns) : '' }">
                <label for="${field.name}" class="invisilabel">${field.label}</label>
                ${calculateField(field)}
            </div>`;
}

const calculateField = (field) => {
    if (field.type === 'text') { return generateTextField(field); }
    if (field.type === 'any') { return generateTextField(field); }
    if (field.type === 'city') { return generateTextField(field); }
    if (field.type === 'phone') { return generatePhoneField(field); }
    if (field.type === 'suffix') { return generateSuffix(field); }
    if (field.type === 'email') { return generateEmail(field); }
    if (field.type === 'zip') { return generateZip(field); }
    if (field.type === 'state') { return generateState(field); }
    if (field.type === 'dropdown') { return generateDropdown(field); }
    if (field.type === 'radio') { return generateRadio(field); }
    return `Unknown type "${field.type}"`
}

const generateTextField = (field) => {
    return `<input type="${field.type}" name="${field.name}" id="${field.name}" value="<?php echo $_POST['${field.name}'];?>" maxlength="30" placeholder="${field.placeholder}" ${field.required ? 'required' : ''}>`;
}

const generatePhoneField = (field) => {
    return `<input type="tel" name="${field.name}" maxlength="14" value="<?php echo $_POST['${field.name}'];?>" ${field.required ? 'required' : ''}>`;
}

const generateSuffix = (field) => {
    return `<select name="${field.name}" id="${field.name}" ${field.required ? 'required' : ''}>
                <option value="">${field.placeholder}</option>
                <option value="Jr."<?php if ($_POST['${field.name}'] == 'Jr.') { echo ' selected'; }?>>Jr.</option>
                <option value="Sr."<?php if ($_POST['${field.name}'] == 'Sr.') { echo ' selected'; }?>>Sr.</option>
                <option value="II"<?php if ($_POST['${field.name}'] == 'II') { echo ' selected'; }?>>II</option>
                <option value="III"<?php if ($_POST['${field.name}'] == 'III') { echo ' selected'; }?>>III</option>
                <option value="IV"<?php if ($_POST['${field.name}'] == 'IV') { echo ' selected'; }?>>IV</option>
                <option value="V"<?php if ($_POST['${field.name}'] == 'V') { echo ' selected'; }?>>V</option>
            </select>`;
} 

const generateEmail = (field) => {
    return `
        <input type="email" name="${field.name}" maxlength="50" value="<?php echo $_POST['${field.name}'];?>" placeholder="${field.placeholder}" ${field.required ? 'required' : ''}>
        <label for="email_confirm" class="invisilabel">Retype Email</label>
        <input type="email" name="email_confirm" id="email_confirm" maxlength="50" value="<?php echo $_POST['${field.name}'];?>" placeholder="Retype Email" ${field.required ? 'required' : ''}>
    `;
} 

const generateZip = (field) => {
    return `<input type="text" name="${field.name}" id="${field.name}" maxlength="5" value="<?php echo $_POST['${field.name}'];?>" placeholder="${field.placeholder}" ${field.required ? 'required' : ''}></input>`;
} 

const generateState = (field) => {
    return `<?php create_dropdown('State', '${field.required ? 'required' : ''}', $_POST['${field.name}']);?>`;
} 

const generateDropdown = (field) => {
    return `<select name="${field.name}">
                <option ${field.required ? 'required' : ''} value=""<?php
                if (!$_POST['${field.name}']) {
                    echo ' selected';
                }
                ?>>${field.placeholder}</option>
                ${field.options.map(option => `<option <?php if ($_POST['${field.name}'] == '${option.value}') { echo 'selected'; } ?>>${option.label}</option>`).join('')}
            </select>`;
} 

const generateRadio = (field) => {
    return `${field.options.map(option => 
        `<label>
            <input name="${field.name}" value="${option.value}" data-label="${option.label}" data-price="${option.price && option.price !== 0 ? option.price : '0' }" type="radio" <?php if ($_POST['${field.name}'] == '${option.value}') { echo 'checked'; } ?> ${field.required ? 'required' : ''}> ${option.label} ${option.price && option.price !== 0 ? '($' + option.price +') ' : '' }
        </label>
        <i class="tooltip"><?php echo $app['helper.icon']->icon('help-circle');?><span class="ttbubble">${option.tooltip}</span></i>`)
        .join('<br>')}
    `;
}

const generateRequired = (group) => {
    return group.fields.find(i => i.required) ? '<?php asterisk();?>' : ''
}