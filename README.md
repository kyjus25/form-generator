# Form Generator

## Prerequesites
* Node

## Usage
* Create input.json and run `node index.js`
* `form.php` will be created in the root directory

## Field Type Validation
* **any** - Input text box, can contain any character or number, only checks if the field has value
* **text** - Input text box, no numbers allowed
* **suffix** - Select dropdown of name suffixes
* **radio** - Radio selection, requires options
* **city** - Input text box, must pass city regex
* **zip** - Input text box, limit 5 numbers
* **state** - Select dropdown of states
* **phone** - Input text box, requires 10 numbers after stripping
* **email** - Input text box for both email and confirm email, must pass email regex and must match

## Important Notes

In order to generate an email, the following fields **MUST** be included in every form:
* FirstName
* LastName
* Email (of type email)
* Phone

## Type Definitions

### Input
```
    "form": {
        "title": string,
        "to": string
    },
    "paypal": {
        "client": string
    },
    "bricks": Brick[]
```

### Brick
```
    {
        "label": "string",
        "groups": Group[]
    }
```

### Group
```
    {
        "label": string,
        "columns": number,
        "fields": Field[]
    }
```

### Field
```
    {
        "label": string, 
        "name": string, // snake_case 
        "type": "radio", // any, text, suffix, radio, city, zip, state, phone, email
        "options?": Option[],
        "required": boolean
    }
```

### Option
```
     {
     "label": string, 
     "value": string, // snake_case
     "price?": number, // Supports float values, defaults to 0
     "tooltip?": string // "<i>Header</i>Description"
     }
```
