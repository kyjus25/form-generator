const fs = require('fs');
const email = require('./partials/email');
const form = require('./partials/form');
const validation = require('./partials/validation');

let payload = '';
payload += email.generate();
payload += form.generate();
payload += validation.generate();

fs.writeFile('./form.php', payload, (err) => {
    if (err) return console.log(err);
});